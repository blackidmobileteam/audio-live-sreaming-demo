package com.example.audiostreamingexample;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.SeekBar;

public class MainActivity extends Activity {
	
	SeekBar fileProgress;
	ImageButton playPauseBtn;
	
	private MediaPlayer mediaPlayer;
	private int mediaFileLengthInMilliseconds;
	private final Handler handler = new Handler();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		fileProgress	= (SeekBar)findViewById(R.id.audio_seek_bar);
		playPauseBtn	= (ImageButton)findViewById(R.id.start_stop_audio);
		
		mediaPlayer		= new MediaPlayer();
		
		fileProgress.setMax(99);
		fileProgress.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				
				if(mediaPlayer.isPlaying()){
					SeekBar sb	= (SeekBar)v;
					int playPositionInMillisecconds = (mediaFileLengthInMilliseconds/100)*sb.getProgress();
					mediaPlayer.seekTo(playPositionInMillisecconds);
				}
				
				return false;
			}
		});

		
		mediaPlayer.setOnBufferingUpdateListener(new OnBufferingUpdateListener() {
			
			@Override
			public void onBufferingUpdate(MediaPlayer mp, int percent) {
				// TODO Auto-generated method stub
				fileProgress.setSecondaryProgress(percent);
			}
		});
		
		mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
			
			@Override
			public void onCompletion(MediaPlayer mp) {
				// TODO Auto-generated method stub
				playPauseBtn.setImageResource(R.drawable.start_audio);
			}
		});
		
		playPauseBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					//mediaPlayer.setDataSource("http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3"); // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
					mediaPlayer.setDataSource("http://api.soundcloud.com/tracks/160229478/stream?client_id=0ab4e5a2d2e49f135f5adf98445e9196");
					mediaPlayer.prepare(); // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer. 
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				mediaFileLengthInMilliseconds = mediaPlayer.getDuration(); // gets the song length in milliseconds from URL
				
				if(!mediaPlayer.isPlaying()){
					mediaPlayer.start();
					playPauseBtn.setImageResource(R.drawable.start_audio);
				}else {
					mediaPlayer.pause();
					playPauseBtn.setImageResource(R.drawable.stop_audio);
				}
				
				primarySeekBarProgressUpdater();
			}
		});
		
	}
	
	private void primarySeekBarProgressUpdater() {
    	fileProgress.setProgress((int)(((float)mediaPlayer.getCurrentPosition()/mediaFileLengthInMilliseconds)*100)); // This math construction give a percentage of "was playing"/"song length"
		if (mediaPlayer.isPlaying()) {
			Runnable notification = new Runnable() {
		        public void run() {
		        	primarySeekBarProgressUpdater();
				}
		    };
		    handler.postDelayed(notification,1000);
    	}
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
